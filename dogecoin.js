const https = require('https')
const puppeteer = require('puppeteer');
var dateTime = require('node-datetime');
const express = require('express')
const app = express();
var rate = 0.313530;

var browser;

(async () => {

  process.on('unhandledRejection', (reason, p) => {
    console.error('Unhandled Rejection at: Promise', p, 'reason:', reason);
    // browser.close();
  });


  // prod mode
  browser = await puppeteer.launch({
    headless: false,
    args: [
      '--window-size=1920,1080',
      '--no-sandbox', '--disable-setuid-sandbox'
    ],
  });

  console.log('Browser has been started');

  const pages = await browser.pages();

  const page = pages[0];

  await page.setViewport({
    width: 1920,
    height: 1080,
  });

  watchCrypto(page, 'DOGE');

})();




app.get('/view', async (req, res) => {

  const pages = await browser.pages();

  const page = pages[0];

  await page.setViewport({
    width: 1920,
    height: 1080,
  });

  // await page._client.send('Emulation.clearDeviceMetricsOverride');

  await page.goto("https://www.ip2location.com/demo")

  let fileName = await takeShot(page, 'shots');


  // fileName = __dirname + '/' + fileName;

  console.log(fileName);

  res.sendFile(fileName, {}, function (err) {
    if (err) {
      // next(err)
      console.log(err);
    } else {
      console.log('Sent:', fileName)
    }
  });

  // res.send('view');

});


const takeShot = async (page, folder = '') => {
  var dt = dateTime.create();
  var formatted = dt.format('Y-m-d_H:M:S');
  // console.log(formatted);
  let fileName = '';
  if (folder == '') {
    fileName = `imgs/${formatted}.png`;
    await page.screenshot({ path: fileName });
  } else {
    fileName = `imgs/${folder}/${formatted}.png`;
    await page.screenshot({ path: fileName });
  }

  console.log(__dirname + '/' + fileName);

  return __dirname + '/' + fileName;
}

const watchCrypto = async (page, crypto = 'DOGE') => {


  if (await page.url() != 'https://www.binance.com/en/markets') {
    await page.goto("https://www.binance.com/en/markets");
  }

  await page.waitForSelector('div[class="css-9d80hs"]');

  const clicked = await page.evaluate(() => {
    // console.log(document.querySelectorAll('div[class="css-9d80hs"]')[1].querySelector('div'));
    if (document.querySelectorAll('div[class="css-9d80hs"]') && document.querySelectorAll('div[class="css-9d80hs"]')[1] && document.querySelectorAll('div[class="css-9d80hs"]')[1].querySelector('div')) {
      document.querySelectorAll('div[class="css-9d80hs"]')[1].querySelector('div').click();
      return true;
    } else {
      return false;
    }

  });

  await page.waitForSelector('.css-4cffwv', { visible: true, timeout: 0 });

  await page.exposeFunction('puppeteerLogMutation', (value) => {
    // console.log(value);
    rate = parseFloat(value.replace('$', ''));

    // console.log(rate);
  });

  if (await clicked) {
    await page.evaluate(() => {
      function querySelectorIncludesText(selector, text) {
        return Array.from(document.querySelectorAll(selector))
          .find(el => el.textContent.includes(text));
      }
      // const target = document.querySelectorAll('.css-4cffwv')[0].parentElement;
      // const elements = document.querySelectorAll('.css-4cffwv');
      let target = querySelectorIncludesText('.css-4cffwv', 'Dogecoin');


      console.log(target);
      if (target) {
        target = target.querySelectorAll('div > div[data-area="left"]')[1];
        console.log(target);
        const observer = new MutationObserver(mutations => {
          for (const mutation of mutations) {
            console.log(mutation);
            puppeteerLogMutation(mutation.target.nodeValue);

          }
        });
        observer.observe(target, { childList: true, subtree: true, characterDataOldValue: true });
      }
      // elements.forEach((item) => {
      //   // console.log(item.textContent);
      // });
      // console.log(target);
    });
    console.log(clicked);
  }

  function intervalFunc() {
    // console.log(rate);
    console.log('running...');

    if (rate < 0.25000) {
      const data = JSON.stringify(
        {
          name: "Binance",
          email: "parmonov98@yandex.ru",
          message: rate,
          lang: "uz",
          isSite: "yes",
          via: "https://binance.com/en/markets"
        }
      )

      const options = {
        hostname: 'parmonov98.uz',
        port: 443,
        path: '/contact.php',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          // 'Content-Length': data.length
        }
      }

      const req = https.request(options, res => {
        console.log(`statusCode: ${res.statusCode}`)

        res.on('data', d => {
          process.stdout.write(d)
        })
      })

      req.on('error', error => {
        console.error(error)
      })

      req.write(data)
      req.end()
    }

  }

  setInterval(intervalFunc, 60000);

}

app.listen(3006);
console.log('Node App is running on port 3006');
